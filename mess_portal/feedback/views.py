from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from feedback.models import feedback
from django.core.urlresolvers import reverse
from blog.views import user_serializer
from rest_framework.decorators import api_view
from rest_framework.response   import Response
from rest_framework import status,serializers 

# Create your views here.
@api_view(['POST'])
def feedback_index(request):
	user_Serializer = user_serializer ()
	try :
		usr = user_Serializer.create(**request.data)
		return render (request,"feedback/index.html")
	except :
		return HttpResponseRedirect(reverse('signin:signin')) 

#===========================================================================================================================================================================================================
#===========================================================================================================================================================================================================

class feed():
	def __init__(self,subject,Text1):
		self.sub = subject
		self.text = Text1

class feed_serializer(serializers.Serializer):
	subject = serializers.CharField()
	Text1   = serializers.CharField()
	def create (self,**data):
		r_object = feed(**data)
		return r_object

class thanks_message ():
	message = "Thanks!!!"

class message_serializer(serializers.Serializer):
	message = serializers.CharField()

@api_view(['POST'])
def handling_data (request):
	data_serializer = feed_serializer ()
	fb = data_serializer.create(**request.data)
	feedback_object = feedback()
	feedback_object.subject = (fb.sub)
	feedback_object.feedback_text = (fb.text)
	feedback_object.save()
	message_object = thanks_message()
	message = message_serializer(message_object)
	return Response(status = status.HTTP_200_OK,data=message.data)
