from rest_framework.decorators import api_view
from rest_framework import serializers, status
from rest_framework.response import Response

from django.shortcuts import render,  get_object_or_404

from django.http import HttpResponse,HttpResponseRedirect
from django.utils import timezone
from .models import Day, Food,ToOptOut
from django.core.urlresolvers import reverse
import datetime

def index(request):
    day_list = Day.objects.all()
    context = {'day_list': day_list}
    return render(request, 'mess_menu/index.html', context)

def detail(request, day_id):
    day = get_object_or_404(Day,pk = day_id)
    return render(request,'mess_menu/detail.html',{'day':day})

def today(request):
    foodcount1 = User.objects.count() - ToOptOut.objects.filter(date=datetime.date.today(),optedOut1=True).count()
    foodcount2 = User.objects.count() - ToOptOut.objects.filter(date=datetime.date.today(),optedOut2=True).count()
    foodcount3 = User.objects.count() - ToOptOut.objects.filter(date=datetime.date.today(),optedOut3=True).count()
    day_list = Day.objects.all()
    date = timezone.now()
    today_name = date.today().strftime("%A")
    count = []
    count.append(foodcount1)
    count.append(foodcount2)
    count.append(foodcount3)			
    context = {
        'count':count,
        'day_list': day_list,
        'today_name': today_name,
    }
    return render(request, 'mess_menu/today.html', context)


def opt_out (request):
	if request.user.is_authenticated():
		username  = request.user
		user = User.objects.get(username= username)
		optOut = user.tooptout_set.get(date=datetime.date.today())
		context = {'opted_out_1':opOut.optedOut1,
			'opted_out_2':opOut.optedOut2,
			'opted_out_3':opOut.optedOut3}
		return render (request,'mess_menu/opt_out.html',context)
	else:
		return HttpResponseRedirect(reverse('signin:signin')) 


def decrement (request):
	username  = request.user
	user = User.objects.get(username= username)
	optOut = user.tooptout_set.get(date=datetime.date.today())
	if 'breakfast'  in request.POST:
		optOut.optOut1 = True
	if 'lunch'  in request.POST:
		optOut.optOut2 = True
	if 'dinner'  in request.POST:
		optOut.optOut3 = True
	optOut.save()
	return HttpResponseRedirect (reverse('mess_menu:today'))
    
	

@api_view(['GET'])
def day_list_api(request):
    
    day_model_list = Day.objects.all()
    
    day_python_list = []
    for day in day_model_list:
        python_day = Typeclass(day=day.day_name)
        day_python_list.append(python_day)

    serializer_list = Serializer(day_python_list, many=True)
    
    return Response(status=status.HTTP_200_OK, data=serializer_list.data)



class Typeclass():
    def __init__(self, day):
        self.day = day    


class Serializer(serializers.Serializer):
     day = serializers.CharField()







    
class menutypeclass():
    def __init__(self, food):
        self.food = food 

class daytypeclass():
    def __init__(self, day, menu):
        self.day = day
        self.menu = menu  

class foodserializer(serializers.Serializer):
    food = serializers.CharField()

class daymenu_serializer(serializers.Serializer):
    day = serializers.CharField()
    menu = foodserializer(many= True)  


@api_view(['GET'])   
def menu_api(request, day_id):
    
    day = Day.objects.get(id = day_id)
    menu = day.food_set.all()
    menu_list = []
    
    for x in menu:
        x_py = menutypeclass(food = x.food_time)
        menu_list.append(x_py)

    py_object = daytypeclass(day = day.day_name, menu = menu_list)
    py_ob_ser = daymenu_serializer(py_object)  

    return Response(status=status.HTTP_200_OK, data= py_ob_ser.data)


@api_view(['GET'])   
def todaymenu_api(request):

    date = timezone.now()
    today_name = date.today().strftime("%A")
    print(today_name)
    i=1
    
    
    for day in Day.objects.all():
        if today_name == day.day_name:
            print(day.day_name)
            day_id = i
        i =i+1     


    day = Day.objects.get(id = day_id)
    menu = day.food_set.all()
    menu_list = []
    for x in menu:
        x_py = menutypeclass(food = x.food_time)
        menu_list.append(x_py)

    py_object = daytypeclass(day = day.day_name, menu = menu_list)
    py_ob_ser = daymenu_serializer(py_object)  

    return Response(status=status.HTTP_200_OK, data= py_ob_ser.data)









    
















    
    
    

    
