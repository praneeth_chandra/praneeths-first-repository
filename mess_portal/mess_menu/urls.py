from django.conf.urls import url

from . import views
app_name = 'mess_menu'
urlpatterns = [
		url(r'^todaymenu_api', views.todaymenu_api, name='todaymenu_api'),
		url(r'^menu_api/(?P<day_id>[0-9]+)/$', views.menu_api, name='menu_api'),
		url(r'^day_list_api', views.day_list_api, name='day_list_api'),
        url(r'^today', views.today, name='today'),
        url(r'^$', views.index, name='index'),
	    url (r'opt_out/handle',views.decrement,name='handle'),
		url (r'opt_out',views.opt_out,name='opt_out' ),
        url(r'^(?P<day_id>[0-9]+)/$', views.detail, name='detail'),
        ]
