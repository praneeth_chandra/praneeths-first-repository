from django.conf.urls import url
from signup import views

app_name = 'signup'

urlpatterns = [
	url(r'^$',views.signup,name='signup'),
	url(r'^handling',views.handling,name='handling'),
	url(r'^signup_api',views.signup_api,name='signup_api'),
]
