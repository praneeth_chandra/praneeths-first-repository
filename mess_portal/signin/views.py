from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers

from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from mess_menu.models import ToOptOut
import datetime


class Logintypeclass(object):
	def __init__(self,username):
		self.username=username



class LoginSerializers(serializers.Serializer):
	username=serializers.CharField()
	def create(self,**validated_data):
		logintypeclass_object=Logintypeclass(**validated_data)
		return logintypeclass_object


@api_view(['POST'])
def signin_api(request):
    print("about to go to loop")
    li_ser=LoginSerializers()
    li_user=li_ser.create(**request.data)
    user_name=li_user.username
    if User.objects.filter(username=user_name):
        user  = User.objects.get(username = user_name)
        if not user.tooptout_set.filter(date = datetime.date.today()) :
            user.tooptout_set.create (date = datetime.date.today())
        return HttpResponseRedirect(reverse('mess_menu:today'))
    else :
        return render(request,'signin/index.html')
    return Response(status=status.HTTP_200_OK)

class Signintypeclass(object):
	def __init__(self,username,password):
		self.username=username
		self.password=password

class Messagetypeclass(object):
	def __init__(self,message):
		self.message=message
		
class MessageSerializers(serializers.Serializer):
	message=serializers.CharField()
	
class SigninSerializers(serializers.Serializer):
	username=serializers.CharField()
	password=serializers.CharField()
	def create(self,**validated_data):
		signintypeclass_object=Signintypeclass(**validated_data)
		return signintypeclass_object


@api_view(['POST'])
def handling_api(request):
    si_ser=SigninSerializers()
    si_user=si_ser.create(**request.data)
    user_name=si_user.username
    user_password=si_user.password
    user = authenticate(username=user_name,password=user_password)
    if user is not None:
        if user.is_active:
            login(request,user)
            if not user.tooptout_set.filter(date = datetime.date.today()) :
                user.tooptout_set.create (date = datetime.date.today())
            return HttpResponseRedirect(reverse('mess_menu:today'))
        else:
            message=Messagetypeclass("The password is valid, but the account has been disabled!")
            m_s=MessageSerializers(message)
            return Response(data=m_s.data,status=status.HTTP_200_OK)
    else:
        message=Messagetypeclass("The username or password or both were incorrect.")
        m_s=MessageSerializers(message)
        return Response(data=m_s.data,status=status.HTTP_200_OK)
		





# def signin (request):
# 	if request.user.is_authenticated():
# 		if Food_Count.objects.filter(date=datetime.date.today()).count() == 0:
# 			s = Food_Count()
# 			s.food_count_1 = Is.objects.count()
# 			s.food_count_2 = Is.objects.count()
# 			s.food_count_3 = Is.objects.count()
# 			s.date         = datetime.date.today()
# 			s.save()
# 			for usr in Is.objects.all() :
# 				usr.opted_out_1 = False
# 				usr.opted_out_2 = False
# 				usr.opted_out_3 = False
# 				usr.save() 
# 		return HttpResponseRedirect(reverse('mess_menu:today'))
# 	else :
# 		return render(request,'signin/index.html')


# def handling (request):
# 	user = authenticate(username=request.POST['username'],password=request.POST['password'])
# 	if user is not None:
# 		if user.is_active:
# 			login(request,user)
# 			if Food_Count.objects.filter(date=datetime.date.today()).count() == 0:
# 				s = Food_Count()
# 				s.food_count_1 = Is.objects.count()
# 				s.food_count_2 = Is.objects.count()
# 				s.food_count_3 = Is.objects.count()
# 				s.date         = datetime.date.today()
# 				s.save()
# 				for usr in Is.objects.all() :
# 					usr.opted_out_1 = False
# 					usr.opted_out_2 = False
# 					usr.opted_out_3 = False
# 					usr.save() 
# 			return HttpResponseRedirect(reverse('mess_menu:today'))
# 		else:
# 			return HttpResponse("The password is valid, but the account has been disabled!")
# 	else:
# 		return HttpResponse("The username or password or both were incorrect.")


def signout (request):
	logout(request)
	return HttpResponseRedirect(reverse('signin:signin_api'))
	

