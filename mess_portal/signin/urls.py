from django.conf.urls import url
from . import views
app_name = 'signin'

urlpatterns = [
	url(r'^signin_api$',views.signin_api,name='signin_api'),
	url(r'^handling_api$',views.handling_api,name='handling_api'),
	#url(r'^$',views.signin,name='signin'),
	url(r'signout',views.signout,name='signout'),
	#url(r'^handling',views.handling,name='handling'),
]
