from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from blog.models import Question,Answer
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response   import Response
from rest_framework import status,serializers 
# Create your views here.

class user_serializer (serializers.Serializer):
	user = serializers.CharField()
	def create(self,**data):
		robject = User.objects.get(**data)
		return robject

@api_view(['POST'])
def index_api_get (request):
	user_Serializer = user_serializer ()
	try :
		usr = user_Serializer.create(**request.data)
		all_questions = list(Question.objects.all())
		context ={'all_questions':all_questions}
		return render (request,"blog/index.html",context)
	except :
		return HttpResponseRedirect(reverse('signin:signin')) 	 


#===============================================================================================================================================================================================================
#===============================================================================================================================================================================================================

@api_view(['POST'])
def api_detail (request,question_id):
	detail_Serializer = user_serializer ()
	try :
		usr = detail_Serializer.create(request.data['username'])
		question = Question.objects.get(id=question_id)
		all_answers = question.answer_set.all()
		context = {'question':question,
			   'all_answers':all_answers,}
		return render (request,"blog/answer.html",context)
	except :
		return HttpResponseRedirect(reverse('signin:signin')) 

#===============================================================================================================================================================================================================
#===============================================================================================================================================================================================================

def handling(request,question_id):
	question = Question.objects.get(id=question_id)
	answer = Answer()
	answer.answer_text = (request.POST['Text1'])
	answer.question = question
	answer.save()
	return HttpResponseRedirect (reverse('blog:detail',kwargs={'question_id':question_id}))



#===============================================================================================================================================================================================================
#===============================================================================================================================================================================================================

class question_Text ():
	def __init__(self,Text1):
		self.text = Text1

class q_serializer (serializers.Serializer):
	Text1 = serializers.CharField()
	def create (self,**data):
		r_object = question_Text(**data)
		return r_object

@api_view(['POST'])
def add_question(request):
	question = Question()
	question_serializer = q_serializer()
	Question_Text = question_serializer.create(**request.data)
	question.question_text=Question_Text.text
	question.pub_date = timezone.now()
	question.save()
	return HttpResponseRedirect (reverse('blog:index'))

#===============================================================================================================================================================================================================
#===============================================================================================================================================================================================================
		




